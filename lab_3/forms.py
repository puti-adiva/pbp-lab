from django import forms
from django.db import models
from django.db.models import fields
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
    error_messages = {
		'required' : 'Please Type'
	}
    input_name = {
		'type' : 'text',
		'placeholder' : 'Masukkan Nama Kamu'
	}
    input_npm = {
        'type' : 'text',
        'placeholder' : 'Masukkan NPM Kamu'
    }
    input_dob = {
        'type' : 'date',
    }
    name = forms.CharField(label='Nama:', required=True, max_length=27, widget=forms.TextInput(attrs=input_name))
    npm = forms.CharField(label='NPM:', required=True, max_length=27, widget=forms.TextInput(attrs=input_npm))
    dob = forms.DateField(label='DOB:', required=True, widget=forms.DateInput(attrs=input_dob))


