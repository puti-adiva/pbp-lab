### Apa perbedaan antara JSON dan XML?
JSON dan XML adalah format yang digunakan untuk melakukan pertukaran data di internet.
Data yang kita buat pada program kita biasanya dibangun dari data string, number, list,
dan lain-lain. Untuk dikirimkan di internet, data ini perlu dikonstruksi dalam suatu
format. Format JSON dan XML adalah contohnya.

JSON dan XML memiliki perbedaan. Berikut adalah beberapa perbedaannya.
* JSON adalah data format, sedangkan XML adalah markup language.
* JSON menyimpan data dalam bentuk map, sedangkan XML dalam bentuk tree.
* Penulisan JSON menggunakan key-value pairs, sedangkan XML menggunakan named tags.
Akibatnya, file JSON lebih mudah dibaca oleh manusia.
* Kita dapat menambahkan metadata pada XML, sedangkan tidak pada JSON.

### Apa perbedaan antara HTML dan XML?
HTML dan XML, keduanya merupakan markup language. Namun, keduanya memiliki fungsi yang
berbeda. HTML berfungsi untuk menampilkan data pada halaman web serta membuat struktur
halaman web. Fokusnya adalah display atau tampilan web. Di sisi lain, XML berfungsi
untuk menyimpan data dan metadata dengan struktur tertentu. XML juga digunakan dalam
data delivery di internet.