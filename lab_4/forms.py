from django import forms
from django.db import models
from django.db.models import fields
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'fromm', 'title', 'message']
    error_messages = {
		'required' : 'Please Type'
	}
    input_to = {
		'placeholder' : 'Masukkan nama penerima'
	}
    input_from = {
        'placeholder' : 'Masukkan nama pengirim'
    }
    input_title = {
        'placeholder' : 'Masukkan judul' 
    }
    input_message = {
        'placeholder' : 'Masukkan pesan' 
    }
    to = forms.CharField(label='To:', required=True, max_length=27, widget=forms.TextInput(attrs=input_to))
    fromm = forms.CharField(label='From:', required=True, max_length=27, widget=forms.TextInput(attrs=input_from))
    title = forms.CharField(label='Title:', required=False, widget=forms.TextInput(attrs=input_title))
    message = forms.CharField(label='Message:', required=True, widget=forms.Textarea(attrs=input_message))


