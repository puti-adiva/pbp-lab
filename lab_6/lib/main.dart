import 'package:flutter/material.dart';

/*
References:
- https://medium.com/@DakshHub/flutter-displaying-dynamic-contents-using-listview-builder-f2cedb1a19fb
- https://stackoverflow.com/questions/54480641/flutter-how-to-create-forms-in-popup
- https://api.flutter.dev/flutter/widgets/ListView-class.html
- https://api.flutter.dev/flutter/material/Card-class.html
- https://www.youtube.com/watch?v=qp9KtPQBa8Y
- https://www.youtube.com/watch?v=aHT7v_4UBtA
- https://www.youtube.com/watch?v=O7yaoGRNY2E
 */

void main() {
  runApp(const ToDoListApp());
}

class ToDoListApp extends StatelessWidget {
  const ToDoListApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDoListList(),
    );
  }
}

class ToDoListList extends StatefulWidget {
  const ToDoListList({Key? key}) : super(key: key);

  @override
  _ToDoListListState createState() => _ToDoListListState();
}

class _ToDoListListState extends State<ToDoListList> {
  List<String> toDoItems = <String>[
    'Push lab PBP',
    'Checkpoint SDA',
    'Forum SOSI'
  ];
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("To Do"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(20),
            child: TextField(
              controller: controller,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.assignment_rounded),
                  hintText: "Add task..."),
              onSubmitted: (text) {
                toDoItems.add(text);
                controller.clear();
                setState(() {});
              },
            ),
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: toDoItems.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ToDoItem(toDoItem: toDoItems[index]);
                  })),
        ],
      ),
    );
  }
}

class ToDoItem extends StatefulWidget {
  String toDoItem;
  final TextEditingController controller = TextEditingController();

  ToDoItem({Key? key, required this.toDoItem}) : super(key: key);

  @override
  _ToDoItemState createState() => _ToDoItemState();
}

class _ToDoItemState extends State<ToDoItem> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: const Color(0xffa9d4d9),
        child: InkWell(
          splashColor: Colors.lightBlue.withAlpha(30),
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: Stack(
                        children: <Widget>[
                          Form(child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Padding(padding: EdgeInsets.all(8),
                                child: TextFormField(decoration: InputDecoration(labelText: "To Do:"),),
                              ),
                              Padding(padding: EdgeInsets.all(8),
                                child: TextFormField(decoration: InputDecoration(labelText: "Description:"),),
                              ),
                              Padding(padding: EdgeInsets.all(8),
                                child: Checkbox(value: isChecked, onChanged: (bool? value) { setState(() {
                                  isChecked = value!;
                                }); },),
                              ),
                              Padding(padding: EdgeInsets.all(8),
                                child: TextButton(onPressed: () {
                                  print("Button clicked");
                                }, child: const Text("Save")),
                              ),
                            ],
                          )),
                        ]
                    ),
                  );
                });
          },
          child: SizedBox(
            height: 50,
            child: Center(child: Text(widget.toDoItem)),
          ),
        ),
      ),
    );
  }
}